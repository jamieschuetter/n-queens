from pprint import pprint

def is_in_danger(board, row_index, column_index):
    #check the row for a queen
    #print(board, row_index, column_index)
    for index, value in enumerate (board[row_index]) :      # print(value, value==0)
        if value == 0 or index == column_index:
            #print("here")
            pass
        else: 
            return True
    #chek a col for a queen
    # for index,row in enumerate (board):
    #     if row[column_index] == 0 or row_index:
    #         pass
    #     else:
    #         return True
    # after checking row and col return false ... not in danger
    return False

def place_queen_in_column(board, column_index):
    #base case
    if column_index >= 4:
        return
    for row_number in range(4):
        board[row_number][column_index] = 1
        if is_in_danger(board, row_number, column_index):
        #if in danger remove it
            board[row_number][column_index] =0
        else:
            place_queen_in_column(board, column_index +1)



def four_queens():
    board = [
        [1,0,0,0],
        [0,0,0,0],
        [0,0,0,0],
        [0,0,0,0],
    ]

    place_queen_in_column(board, 1)
    return board


if __name__ == "__main__":
    board = four_queens()
    pprint(board, width=20)